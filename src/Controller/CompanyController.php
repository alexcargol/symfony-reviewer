<?php

namespace App\Controller;


use App\Entity\Category;
use App\Entity\Company;
use App\Entity\Review;
use App\Form\ReviewType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractController
{
    /**
     * @Route("/company/{id}", name="company")
     */
    public function index($id)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $company = $entityManager->getRepository(Company::class)->find($id);
        $avgGrade = $company->getGrade();
        $reviews = $company->getReviews();
                $categories = $entityManager->getRepository(Category::class)->findAll();
        return $this->render('company/index.html.twig', [
           'company' => $company, 'grade' =>$avgGrade, 'categories' => $categories, 'reviews' =>$reviews
        ]);
    }

    /**
     * @Route("/add-review/{id}", name="add-review")
     */
    public function createReview(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $review = new Review();


        $company = $entityManager->getRepository(Company::class)->find($id);
        $review->setCompany($company);
        //$review->setUser($company);
        $form = $this->createForm(ReviewType::class, $review);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $review = $form->getData();
            $entityManager->persist($review);
            $entityManager->flush();

        }
            return $this->render('company/addReview.html.twig',['form'=>$form->createView()]);

    }

}
