<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Company;
use Doctrine\DBAL\Query\QueryBuilder;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="category")
     */
    public function index(Request  $request, PaginatorInterface $paginator)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $companies =  $entityManager->getRepository(Company::class)->findAll();
        $categories = $entityManager->getRepository(Category::class)->findAll();
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = $entityManager->createQueryBuilder();
            $queryBuilder->SELECT('c')
                ->from(Company::class, 'c');
            $pagination = $paginator->paginate(
                $queryBuilder, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                6/*limit per page*/
            );

        return $this->render('category/index.html.twig', [
            'companies' => $companies, 'categories' => $categories, 'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/category-id/{id}", name="category-id")
     */
    public function indexId(Request  $request, PaginatorInterface $paginator, $id)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $oneCategory = $entityManager->getRepository(Category::class)->find($id);
        $cat_name = $oneCategory->getName();
       // $queryBuilder = $oneCategory->getCompanies();
        $categories = $entityManager->getRepository(Category::class)->findAll();
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $entityManager->createQueryBuilder();
        $queryBuilder->SELECT('c')
            ->from(Company::class, 'c')
            ->where('c.cat = ?1')
            ->setParameter(1, $id);
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return $this->render('category/catId.html.twig', [
            'categories' => $categories, 'pagination' => $pagination, 'cat_name' => $cat_name
        ]);
    }

}
