<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Company;
use Doctrine\DBAL\Query\QueryBuilder;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

class PrimoController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->render('primo/index.html.twig', [
            'admin' => 'Alex is Admin',
        ]);
    }
    /**
     * @Route("/search", name="search")
     */

    public function indexSearch(Request  $request, PaginatorInterface $paginator)
    {
        $keyword = $_POST["keyword"];
        $entityManager = $this->getDoctrine()->getManager();
        $categories = $entityManager->getRepository(Category::class)->findAll();

        /** @var QueryBuilder $queryBuilder */

        $queryBuilder = $entityManager->createQueryBuilder();
        $queryBuilder->SELECT('c')
            ->from(Company::class, 'c')
            ->where('c.name like ?1')->orWhere('c.url like ?1')->orWhere('c.content like ?1')
            ->setParameter(1, '%'.$keyword.'%');


        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            3/*limit per page*/
        );

        return $this->render('primo/search.html.twig', [
            'found' => $keyword, 'categories' => $categories, 'pagination' => $pagination
        ]);
    }


    /**
     * @Route("services", name="services")
     */
    public function servicesIndex()
    {
        return $this->render('primo/services.html.twig', [
            'admin' => 'Alex is Admin',
        ]);
    }

    /**
     * @Route("contact", name="contact")
     */
    public function contactForm()
    {
        return $this->render('primo/contact.html.twig', [
            'admin' => 'Alex is Admin',
        ]);
    }
}
