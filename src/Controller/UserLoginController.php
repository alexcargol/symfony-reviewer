<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserLoginController extends AbstractController
{
    /**
     * @Route("/login/user", name="user_login")
     */
    public function index()
    {
        return $this->render('user_login/index.html.twig', [
            'controller_name' => 'UserLoginController',
        ]);
    }

    /**
     * @Route("/login/company", name="company_login")
     */
    public function index2()
    {
        return $this->render('user_login/companyLogin.html.twig', [
            'controller_name' => 'CompanyLoginController',
        ]);
    }

}
